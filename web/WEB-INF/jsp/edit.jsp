<%-- 
    Document   : edit
    Created on : 2019-3-2, 18:59:59
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Items Page</title>
        <link href="css/main.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <h1 id="homeTitle">Edit item</h1>
        <form action="edit.do" method="POST">
            <table>
                <tr>
                    <th>Auction #</th>
                    <td>${item.id}
                    <input type="hidden" name="id" value="${item.id}"/></td>
                </tr>
                <tr>
                    <th>Auction Type</th>
                    <td><input type="text" name="title" id="title" size="20"
                               value="${item.title}"
                               style="background-color: lightgrey;"/></td>
                </tr>
                <tr>
                    <th>Seller</th>
                    <td>${item.author}
                    <input type="hidden" name="author" value="${item.author}"/></td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td><input type="text" name="body" id="body" size="60" value="${item.body}" style="background-color: lightgrey;"/></td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td><select name="categoryId">
                            <option value="-1">-</option>
                            <c:forEach items="${categories}" var="category">
                                <c:choose>
                                    <c:when test="${ item.categoryId == null }">
                                        <option value="${ category.id }">${ category.name }</option>
                                    </c:when>
                                    <c:when test="${ item.categoryId.id == category.id }">
                                        <option value="${ category.id }" selected="selected">${ category.name }</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${ category.id }">${ category.name }</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
            </table>
            <button>Save</button>
        </form>
    </body>
</html>
