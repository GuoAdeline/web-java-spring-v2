<%-- 
    Document   : login
    Created on : 2019-3-2, 16:16:00
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html lang="fr-fr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AuctionMan Connection Page</title>
        <link rel="stylesheet" type="text/css" media="screen" href="css/login.css"/>
    </head>
    <body>
        <form:form action="login.do" method="POST" >
            <h1>Auctions Login</h1>
            <p><input type="text" name="login" /></p>
            <p><input type="password" name="passw" /></p>
            <p><button type="submit" >Login</button></p>
        </form:form>
    </body>
</html>
