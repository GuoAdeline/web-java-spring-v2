<%-- 
    Document   : items
    Created on : 2019-2-28, 22:42:56
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Items Page</title>
        <link href="css/main.css" type="text/css" rel="stylesheet" />
        <script src="js/main.js" type="text/javascript"></script>
        <script type="text/javascript" language="javascript" src="js/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <h1 id="homeTitle">List of items</h1>
        <table>
            <tr>
                <th>Auction #</th>
                <th>Auction type</th>
                <th>Seller</th>
                <th>Description</th>
                <th>Category</th>
                <th>Action</th>
            </tr>
            <c:forEach var="item" items="${itemsList}">
                <tr>
                    <td><c:out value="${ item.id }"/></td>
                    <td><c:out value="${ item.title }"/></td>
                    <td><c:out value="${ item.author }"/></td>
                    <td><c:out value="${ item.body }"/></td>
                    <td><c:if test="${ item.categoryId != null }">${ item.categoryId.name }</c:if></td>
                    <td><input type="button" name="delete" value="delete" 
                               onclick="deleteLine(this, ${item.id});" />
                    <form:form action="edit.do" method="GET">
                        <input type="hidden" name="id" value="${item.id}"/>
                        <button>edit</button>
                    </form:form>
                    </td>
                </tr>
            </c:forEach>
            <form:form action="add.do" method="POST">
                <tr id="addNew">
                    <td></td>
                    <td><input type="text" name="title" id="title" size="20" style="background-color: lightgrey;"/></td>
                    <td><input type="text" name="author" id="author" size="20" style="background-color:lightgrey;"/></td>
                    <td><input type="text" name="body" id="body" size="60" style="background-color: lightgrey;"/></td>
                    <td><select name="categoryId">
                            <option value="-1">-</option>
                            <c:forEach items="${ categories }" var="category">
                            <option value="${ category.id }">${ category.name }</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td style="text-align: center"><button>add</button></td>
                </tr>
            </form:form>
        </table>
    </body>
</html>
