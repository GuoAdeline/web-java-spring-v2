/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function getMyTRTag(ref) {
    var theTDRef = ref;
    var found = false;
    while ((theTDRef !== null) && (! found)) {
        if (theTDRef.nodeType === 3) {
            theTDRef = theTDRef.parentNode;
        } else if (theTDRef.tagName === "TR") {
            found = true;
        } else {
            theTDRef = theTDRef.parentNode;
        }
    }
    return theTDRef;
}

function deleteLine(theRef, id) {
    $.ajax({
        url: "AJAX.do",
        data: {
            "id":id,
            "action": "delete"
        },
        method: "POST",
        success: function( result ) {
            var theTR = getMyTRTag(theRef);
            if (theTR !== null) {
                var parent = theTR.parentNode;
                parent.removeChild(theTR);
            }
        },
        error: function(resultat, statut, erreur) {
            console.log("error");
        }
    });
}

