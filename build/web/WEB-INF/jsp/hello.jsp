<%-- 
    Document   : hello
    Created on : 2019-2-27, 17:01:59
    Author     : GUO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!--<h1>Hello World! <c:out value="${anUser['login']}"/> <c:out value="${anUser['passw']}"/>!</h1>-->
        <h1>Hello World! <c:out value="${myLogin}"/> <c:out value="${myPasswd}"/>!</h1>

    </body>
</html>
