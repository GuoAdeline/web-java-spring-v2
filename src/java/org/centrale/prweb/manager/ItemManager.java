/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.manager;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author GUO
 */
public class ItemManager {
    private static final String PUNAME = "prwebSPRINGPU";
    private static EntityManagerFactory emf = null;
    private static EntityManager em;
    private static ItemManager theItemManager;
    
    private ItemManager() {
        // Create the factory
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory(PUNAME);
        // Clear cache
        Cache theCache = emf.getCache();
        theCache.evictAll();
        // Create unique entity manager
        em = emf.createEntityManager();
        em.clear();
        }
    }
     public static ItemManager getManager() {
         if (theItemManager == null) {
             theItemManager = new ItemManager();
         }
         return theItemManager;
    }
     
    public static EntityManager getEntityManager() {
        ItemManager manager = getManager();
        return em;
    }
    
    public static EntityTransaction getTransaction() {
        ItemManager manager = getManager();
        return em.getTransaction();
    }
    
     
}
