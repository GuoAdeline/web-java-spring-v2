/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.controller;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.prweb.dao.CategoryDAO;
import org.centrale.prweb.dao.DAOFactory;
import org.centrale.prweb.dao.ItemDAO;
import org.centrale.prweb.items.Category;
import org.centrale.prweb.items.Item;
import org.centrale.prweb.manager.ItemManager;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author GUO
 */
@Controller
public class EditController {
    
     public EditController() {
     }
     
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleEdit(HttpServletRequest request,
            HttpServletResponse response) {
        String idSTR = request.getParameter("id");
        int id = Integer.parseInt(idSTR);
        
        ItemDAO itemDAO = DAOFactory.getItemDAO();
        Item item = itemDAO.findById(id);
        
        CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
        Collection<Category> categoriesList = categoryDAO.getAll();
        
        ModelAndView result = new ModelAndView("edit");
        result.addObject("item", item);
        result.addObject("categories", categoriesList);
        return result;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(HttpServletRequest request,
            HttpServletResponse response) {
        // Get parameters
        String idSTR = request.getParameter("id");
        int id = Integer.parseInt(idSTR);
        String categoryIdSTR = request.getParameter("categoryId");
        int categoryId = Integer.parseInt(categoryIdSTR);
        
        ItemDAO itemDAO = DAOFactory.getItemDAO();
        CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
        
        // Save modification
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        Item item = itemDAO.findById(id);
        item.setAuthor(request.getParameter("author"));
        item.setTitle(request.getParameter("title"));
        item.setBody(request.getParameter("body"));
        
        Category category = categoryDAO.findById(categoryId);
        item.setCategoryId(category);
        em.flush();
        transaction.commit();
        
        Collection<Item> itemsList = itemDAO.getAll();
        Collection<Category> categoriesList = categoryDAO.getAll();
       
        ModelAndView result = new ModelAndView("items");
        result.addObject("itemsList", itemsList);
        result.addObject("categories", categoriesList);
        return result;
    }

    // Another way to use forms
//    @RequestMapping(method = RequestMethod.POST)
//    public ModelAndView handlePOST(@ModelAttribute("Item") Item item, BindingResult binding,
//            HttpServletRequest request,
//            HttpServletResponse response) {
//        // Get parameters
//        String idSTR = request.getParameter("id");
//        int id = Integer.parseInt(idSTR);
//        String categoryIdSTR = request.getParameter("categoryId");
//        int categoryId = Integer.parseInt(categoryIdSTR);
//        
//        ItemDAO itemDAO = DAOFactory.getItemDAO();
//        CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
//        
//        // Save modification
//        EntityManager em = ItemManager.getEntityManager();
//        EntityTransaction transaction = ItemManager.getTransaction();
//        transaction.begin();
//        Item item = itemDAO.findById(id);
//        item.setAuthor(request.getParameter("author"));
//        item.setTitle(request.getParameter("title"));
//        item.setBody(request.getParameter("body"));
//        
//        Category category = categoryDAO.findById(categoryId);
//        item.setCategoryId(category);
//        em.flush();
//        transaction.commit();
//        
//        Collection<Item> itemsList = itemDAO.getAll();
//        Collection<Category> categoriesList = categoryDAO.getAll();
//       
//        ModelAndView result = new ModelAndView("items");
//        result.addObject("itemsList", itemsList);
//        result.addObject("categories", categoriesList);
//        return result;
//    }
}


