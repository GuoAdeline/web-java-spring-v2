/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.controller;

import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.prweb.dao.CategoryDAO;
import org.centrale.prweb.dao.DAOFactory;
import org.centrale.prweb.dao.ItemDAO;
import org.centrale.prweb.items.Category;
import org.centrale.prweb.items.Item;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author GUO
 */
@Controller
public class ItemController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGet(HttpServletRequest request,
            HttpServletResponse response) {
        ItemDAO itemDAO = DAOFactory.getItemDAO();
        Collection<Item> itemsList = itemDAO.getAll();
        CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
        Collection<Category> categories= categoryDAO.getAll();
        
        ModelAndView result = new ModelAndView("items");
        result.addObject("itemsList", itemsList);
        result.addObject("categories", categories);
        return result;
    }
    
}
