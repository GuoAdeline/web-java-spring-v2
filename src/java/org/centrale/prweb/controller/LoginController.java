/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.controller;

import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.prweb.dao.CategoryDAO;
import org.centrale.prweb.dao.DAOFactory;
import org.centrale.prweb.dao.ItemDAO;
import org.centrale.prweb.items.Category;
import org.centrale.prweb.items.Item;
import org.centrale.prweb.items.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author GUO
 */
@Controller
public class LoginController {
    
    @Autowired
    private LoginValidator validator;
    
    public LoginController() {
        validator = new LoginValidator();
    }
    
    @RequestMapping(method = {RequestMethod.GET,RequestMethod.POST})
    public ModelAndView handleGet(@ModelAttribute("User") User user, BindingResult binding,
            HttpServletRequest request,
            HttpServletResponse response) {
        ModelAndView result = null;
        
        // Check login / password
        // You can use any login/password except void one
//        if ((user.getLogin() != null) && (!user.getLogin().isEmpty())
//                && (user.getPassw() != null) && (!user.getPassw().isEmpty())){
//            ItemDAO itemDAO = DAOFactory.getItemDAO();
//            Collection<Item> itemsList = itemDAO.getAll();
//            
//            CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
//            Collection<Category> categoriesList = categoryDAO.getAll();
//            
//            result = new ModelAndView("items");
//            result.addObject("itemsList", itemsList);
//            result.addObject("categories", categoriesList);
//        } else {
//            result = new ModelAndView("login");
//        }

        // with default identification
        if (validator != null){
            // There id a validator
            validator.validate(user, binding);
        } else {
            binding.reject("Internal error");
        }
        
        if (!binding.hasErrors()) {
            ItemDAO itemDAO = DAOFactory.getItemDAO();
            Collection<Item> itemsList = itemDAO.getAll();
            CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
            Collection<Category> categoriesList = categoryDAO.getAll();
            
            result = new ModelAndView("items");
            result.addObject("itemsList", itemsList);
            result.addObject("categories", categoriesList);
        } else {
            result = new ModelAndView("login");
        }
        return result;
    }
    
    
}