/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.prweb.dao.DAOFactory;
import org.centrale.prweb.dao.ItemDAO;
import org.centrale.prweb.items.Item;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author GUO
 */
@Controller
public class AJAXController {
    
    public AJAXController() {
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handleAjax(HttpServletRequest request,
            HttpServletResponse response) {
        // Get action
        String action = request.getParameter("action");
        
        JSONObject json = new JSONObject();
        if (action != null) {
            if (action.equals("delete")){
                // Action is delete
                // Get ID
                String idSTR = request.getParameter("id");
                int id = Integer.parseInt(idSTR);
                // Remove item
                ItemDAO itemDAO = DAOFactory.getItemDAO();
                Item item = itemDAO.findById(id);
                itemDAO.remove(item);
            }
        }
        // Send request back
        ModelAndView result = new ModelAndView("ajax");
        result.addObject("theResponse", json.toString());
        return result;
    }
}
