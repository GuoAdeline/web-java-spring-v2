/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.controller;

import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.prweb.dao.CategoryDAO;
import org.centrale.prweb.dao.DAOFactory;
import org.centrale.prweb.dao.ItemDAO;
import org.centrale.prweb.items.Category;
import org.centrale.prweb.items.Item;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author GUO
 */
@Controller
public class AddController {
    public AddController() {
    }
    
//    @RequestMapping(method = RequestMethod.POST)
//    public ModelAndView handleHello(HttpServletRequest request,
//            HttpServletResponse response) {
//        ItemDAO itemDAO = DAOFactory.getItemDAO();
//        Item item = new Item();
//        item.setTitle(request.getParameter("title"));
//        item.setAuthor(request.getParameter("author"));
//        item.setBody(request.getParameter("body"));
//        itemDAO.create(item);
//        
//        Collection<Item> itemsList = itemDAO.getAll();
//        ModelAndView result = new ModelAndView("items");
//        result.addObject("itemsList", itemsList);
//        return result;
//    }
    
    // Another way to use forms
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handleHello(@ModelAttribute("Item") Item item, BindingResult bindingItem,
            @ModelAttribute("Category") Category category, BindingResult bindingCateg,
            HttpServletRequest request,
            HttpServletResponse response) {
        ItemDAO itemDAO = DAOFactory.getItemDAO();
        CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
       
        Category cat = categoryDAO.findById(Integer.parseInt(request.getParameter("categoryId")));
        item.setCategoryId(cat);
        itemDAO.create(item);      // create item dans database
        
        Collection<Item> itemsList = itemDAO.getAll();
        Collection<Category> categories = categoryDAO.getAll();
        ModelAndView result = new ModelAndView("items");
        result.addObject("itemsList", itemsList);
        result.addObject("categories", categories);
        return result;
    }
    
}
