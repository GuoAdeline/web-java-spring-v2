/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.controller;

import org.centrale.prweb.items.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author GUO
 */
@Component
public class LoginValidator implements Validator {
    private final String loginDefault = "admin";
    private final String passwdDefault = "admin";
    
    public LoginValidator() {
    }
    
    @Override
    public boolean supports(Class<?> type) {
        return User.class.isAssignableFrom(type);
    }
    
    @Override
    public void validate(Object o, Errors errors) {
        User anUser = (User) o;
        String login = anUser.getLogin();
        String passwd = anUser.getPassw();
        if ((login == null) || (login.equals(""))) {
            ValidationUtils.rejectIfEmpty(errors, "login", "login.empty", "Login non defini");
        } else if (passwd == null) {
            ValidationUtils.rejectIfEmpty(errors, "passw", "passw.empty", "Mot de passe non defini");
        } else if ((! login.equals(loginDefault)) || (!passwd.equals(passwdDefault))) {
            errors.reject("Invalid", "login/password incorrect");
        } else {
            // Pass through
        }
    }
    
}
