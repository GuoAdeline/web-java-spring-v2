/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.dao;

import java.util.Collection;



/**
 *
 * @author GUO
 */
public interface DAO<T, K> {
    // T: the object class  K: the T id class
    public Collection<T> getAll();
    public T findById(K key);
    public void remove(T item);
    public void create(T item);
}
