/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.centrale.prweb.items.Item;
import org.centrale.prweb.manager.ItemManager;

/**
 *
 * @author GUO
 */
public class ItemDAOImpl implements ItemDAO {
    @Override
    public Collection<Item> getAll() {  
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Item.findAll", Item.class);
        Collection theList = theQuery.getResultList();
        return theList;
    }
    
    @Override
    public Item findById(Integer key) {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Item.findById", Item.class);
        theQuery.setParameter("id", key);
        Item item = (Item) theQuery.getSingleResult();
        return item;
    }
    
    @Override
    public Item findById(int id) {
        return findById(new Integer(id));
    }
    
    @Override
    public void remove(Item item) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(item);
        em.flush();
        transaction.commit();
    }
    
    @Override
    public void create(Item item) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.persist(item);
        em.flush();
        transaction.commit();
    }
}
