/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.dao;

import org.centrale.prweb.items.Item;

/**
 *
 * @author GUO
 */
public interface ItemDAO extends DAO <Item, Integer> {
    public Item findById(int id);
}
