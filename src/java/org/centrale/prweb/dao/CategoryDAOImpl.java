/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.dao;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.centrale.prweb.items.Category;
import org.centrale.prweb.manager.ItemManager;

/**
 *
 * @author GUO
 */
public class CategoryDAOImpl implements CategoryDAO {
    @Override
    public Collection<Category> getAll() {  
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Category.findAll", Category.class);
        Collection theList = theQuery.getResultList();
        return theList;
    }
    
    @Override
    public Category findById(Integer key) {
        EntityManager em = ItemManager.getEntityManager();
        Query theQuery = em.createNamedQuery("Category.findById", Category.class);
        theQuery.setParameter("id", key);
        Category category = (Category) theQuery.getSingleResult();
        return category;
    }
    
    @Override
    public Category findById(int id) {
        return findById(new Integer(id));
    }
    
    @Override
    public void remove(Category category) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.remove(category);
        em.flush();
        transaction.commit();
    }
    
    @Override
    public void create(Category category) {
        EntityManager em = ItemManager.getEntityManager();
        EntityTransaction transaction = ItemManager.getTransaction();
        transaction.begin();
        em.persist(category);
        em.flush();
        transaction.commit();
    }
}
