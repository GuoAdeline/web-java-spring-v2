/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.dao;

/**
 *
 * @author GUO
 */
public abstract class DAOFactory {
    
    public static ItemDAO getItemDAO() {
        return new ItemDAOImpl();
    }
    
    public static CategoryDAO getCategoryDAO() {
        return new CategoryDAOImpl();
    }
}
